==========
bilby_pipe
==========

Command line interface
----------------------

The primary user-interface for this code is a command line tool
:code:`bilby_pipe`, for an overview of this see `the user interface
<../user-interface.txt>`_.

.. argparse::
   :module: bilby_pipe.data_analysis
   :func: create_parser
   :prog: bilby_pipe

