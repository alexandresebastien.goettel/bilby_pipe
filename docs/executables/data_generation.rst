=====================
bilby_pipe_generation
=====================

Command line interface for data generation
------------------------------------------

.. argparse::
   :module: bilby_pipe.data_generation
   :func: create_parser
   :prog: bilby_pipe_generation
